# ComputeurAudioAmplifier1W

Computer external speaker 1W each left/right amplifier powered from USB port.

# Idea

The main Idea here is to build a PCB small enough to fit inside a speaker using only 2 wires 1-USB 1-Stereo-Jack.

Setup : 

- On/OFF Power button of course

- 2 x Potentiometers for left & right volume

- selectors for each right/left amplifications if we use PCB filters or not.

(_if not it just give sound "as is" to the speaker or to the speakers enclosure internal filters_)

**note** Currently the values (_specified for the filter part in particular are calculated for 8ohms speakers_) 

# Files

This is Open-Source model, with [MIT License](./LICENSE)

PCB will look like : 

![FACE](./kicad/kicad_up.png)

![SCHEMA](./SCHEMA.png)

![BOTTOM](./kicad/kicad_down.png)

![PCB size](./Size_PCB.png)

# PDF of schema & pcb

[Schema](./schema.pdf)

[PCB](./PCB.pdf)

# idea for the next version 

- Previous version 	: V0.0.0

- Current version 	: V0.1.0

- [X] upgrade version to V0.1.0 adding filters to use enclosures including 3 speakers (_bass/medium/tweeter_)

- [X] use a discriminant frequencies low pass filter, a band pass one, and a high pass one of course 


